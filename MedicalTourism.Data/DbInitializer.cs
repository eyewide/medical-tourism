﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Data {
    public static class DbInitializer {
        public static void Init() {
            System.Data.Entity.Database
                .SetInitializer(new System.Data.Entity.MigrateDatabaseToLatestVersion<TourContext, Migrations.Configuration>());
        }
    }
}
