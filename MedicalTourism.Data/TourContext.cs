﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedicalTourism.Domain;

namespace MedicalTourism.Data {
    public class TourContext : DbContext {
        public TourContext() : base("Name=umbracoDbDSN") { }
        
        public virtual DbSet<Package> Packages { get; set; }

    }
}
