﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Data.Interfaces {
    public interface IEntityRepository<TEntity> : IDisposable {

        IQueryable<TEntity> All { get; }
        TEntity Find(Guid id);
        IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(Guid id);

    }
}
