﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MedicalTourism.Data.Interfaces;
using MedicalTourism.Domain;

namespace MedicalTourism.Data.Repositories {
    public class RateRepository : IEntityRepository<Package> {

        private TourContext _context;
        private bool disposed = false;

        public RateRepository(TourContext context) {
            _context = context;
        }

        public IQueryable<Package> All {
            get {
                return _context.Packages;
            }
        }

        public void Delete(Guid id) {
            var package = _context.Packages.Find(id);
            _context.Packages.Remove(package);
        }


        public Package Find(Guid id) {
            return _context.Packages.Find(id);
        }

        public IEnumerable<Package> FindBy(Expression<Func<Package, bool>> predicate) {
            IEnumerable<Package> results = _context.Packages.AsNoTracking().Where(predicate).ToList();
            return results;
        }

        public void Insert(Package entity) {
            _context.Packages.Add(entity);
        }

        public void Update(Package entity) {
            _context.Entry(entity).State = EntityState.Modified;
        }

        protected virtual void Dispose(bool disposing) {
            if (!this.disposed) {
                if (disposing) {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Save() {
            _context.SaveChanges();
        }

    }
}
