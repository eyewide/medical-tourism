﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Domain {
    public class Package {

        [Key]
        public Guid Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SerialNumber { get; set; }

        public string Title { get; set; }

        [Column(TypeName = "text")]
        public string Description { get; set; }

        public int UmbracoTransferId { get; set; }

        public int UmbracoHotelId { get; set; }

        public int UmbracoTherapyId { get; set; }

        public int UmbracoExcursionId { get; set; }

        public int Persons { get; set; }

        public decimal Price { get; set; }
        
    }
}
