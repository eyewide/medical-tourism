﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedicalTourism.Data;

namespace MedicalTourism.Services {
    public static class InitService {
        public static void Database() {
            DbInitializer.Init();
        }
    }
}
