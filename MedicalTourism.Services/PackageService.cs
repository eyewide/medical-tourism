﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedicalTourism.Data;
using MedicalTourism.Data.Repositories;
using MedicalTourism.Domain;
using MedicalTourism.Services.DTO;

namespace MedicalTourism.Services {
    public class PackageService {

        private TourContext _context;

        public PackageService () { }

        public IEnumerable<PackageDto> All() {

            _context = new TourContext();

            using (RateRepository repo = new RateRepository(_context)) {
                var packages = new List<PackageDto>();
                foreach (var package in repo.All.ToList()) {
                    packages.Add(AutoMapper.Mapper.Map<PackageDto>(package));
                }
                return packages;
            }
        }

        public PackageDto Create(PackageDto package) {

            _context = new TourContext();
            
            var newPackage = AutoMapper.Mapper.Map<Package>(package);

            using (RateRepository repo = new RateRepository(_context)) {
                newPackage.Id = Guid.NewGuid();
                repo.Insert(newPackage);
                repo.Save();
            }

            return AutoMapper.Mapper.Map<PackageDto>(newPackage);

        }

        public PackageDto Get() {

            _context = new TourContext();

            using (var repo = new RateRepository(_context)) {

                var data = repo.All.OrderByDescending(x => x.Price).FirstOrDefault();
                return AutoMapper.Mapper.Map<PackageDto>(data);

            }
            
        }

    }
}
