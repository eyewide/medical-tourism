﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Services.DTO {
    public class PackageDto {

        public Guid Id { get; set; }

        public int SerialNumber { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public TransferDto Transfer { get; set; }

        public HotelDto Hotel { get; set; }

        public TherapyDto Therapy { get; set; }

        public ExcursionDto Excursion { get; set; }

        public int Persons { get; set; }

        public decimal Price { get; set; }

    }
}
