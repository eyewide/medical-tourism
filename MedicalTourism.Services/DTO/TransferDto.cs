﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Services.DTO {
    public class TransferDto {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

    }
}
