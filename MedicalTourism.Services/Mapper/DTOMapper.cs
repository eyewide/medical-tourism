﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalTourism.Services.Mapper {
    public static class DTOMapper {

        public static void Configure() {
            AutoMapper.Mapper.AddProfile(new DTOMapperProfile());
        }
    }
}
