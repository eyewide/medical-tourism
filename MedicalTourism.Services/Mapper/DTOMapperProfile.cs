﻿using AutoMapper;
using MedicalTourism.Domain;
using MedicalTourism.Services.DTO;

namespace MedicalTourism.Services.Mapper {
    public class DTOMapperProfile : Profile {

        public DTOMapperProfile() {
            AutoMapper.Mapper.CreateMap<Package, PackageDto>();
            AutoMapper.Mapper.CreateMap<PackageDto, Package>();
        }

    }
}
