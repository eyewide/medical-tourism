﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using Umbraco.Core;

namespace MedicalTourism.Web.App_Plugins.Dashboard {
    [PluginController("Dashboard")]
    [Tree("Dashboard", "DashboardTree", "Dashboard")]
    public class DashboardTreeController : TreeController {

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings) {
            throw new NotImplementedException();
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings) {

            var nodes = new TreeNodeCollection();

            if (id == Constants.System.Root.ToInvariantString()) {

                //nodes.Add(CreateTreeNode("bookings", id, queryStrings, "Bookings", "icon-list", false, "TourAdmin/TourAdminTree/bookings/-1"));
                nodes.Add(CreateTreeNode("packages", id, queryStrings, "Packages", "icon-chart-curve", false, "Dashboard/DashboardTree/packages/-1"));

            }

            return nodes;

        }

    }
}