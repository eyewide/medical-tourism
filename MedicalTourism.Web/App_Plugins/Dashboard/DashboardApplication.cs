﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.businesslogic;
using umbraco.interfaces;

namespace MedicalTourism.Web.App_Plugins.Dashboard {
    [Application("Dashboard", "Dashboard", "icon-speed-gauge", 16)]
    public class DashboardApplication : IApplication {
    }
}